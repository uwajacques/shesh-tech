<DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{$title}} </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <style>
       
       @page {
            margin-top: 2.5cm;
            margin-bottom: 2.5cm;
            margin-left: 2cm;
            margin-right: 2cm;
            footer: html_letterfooter;
            background-color: white;
        }
    
        @page :first {
            margin-top: 6cm; /* Body margin top */
            margin-bottom: 4cm;
            header: html_letterheader;
            resetpagenum: 1;
            background-color: white;
            margin-header: 2cm; /* Header margin top */
        }

        @page letterfooter {
            margin-top: 8cm;
            margin-bottom: 49cm;
            header: html_letterfooter;
            footer: _blank;
            resetpagenum: 1;
            background-color: lightblue;
        }
    
       body{
        font-family: 'Montserrat', sans-serif;
       }

        h1{
            font-size: 40px;
            font-weight: normal;
        }

        h2{
            font-size: 20px;
            font-weight: normal;
        }

        p{
            font-size: 14px;
        }

        table { 
            border-spacing: 0;
            border-collapse: collapse;
        }

        table.header {
            width: 100%;
        }

        table.header tr .title {
            text-align: left;
            
        }
        table.header tr .logo {
            width: 200px; 
            text-align: right;
        }
        
        table.body-table{
            width: 100%;
        }

        table.body-table tr{
            margin: 0;
            padding: 0;
        }

        table.body-table tr td{
            width: 50%;
            margin: 0;
            padding: 3px;
            font-size: 16px;
            border: 1px solid black;
        }

        table.body-table tr td.left{
            font-size: 16px;
            font-weight: bold;
        }

        table.body-table tr td.right{
            font-size: 16px;
        }

        table.footer tr{
            border: none;
        }

  

    </style>
</head>
<body>
    <htmlpageheader name="letterheader">
        <table class="header">
            <tr>
                <th class="title" ><h1> {{$title}}</h1> </th>
                <th class="logo" >
                    <img style="height: 100px" src="https://sheshtech.com/wp-content/uploads/2017/10/Shesh-Tech-Positive.png">
                </th> 
            </tr>
        </table>
    </htmlpageheader>
    
    <htmlpagefooter name="letterfooter">
        <table class="footer" style="width: 100%">
            <tr style="width: 100%">
                <td style="width: 33%; text-align: left; padding-top: 7px; padding-bottom: 7px; " ><p> {{$title}} | Page  {PAGENO} </p> </td>
                <td style="width: 33%; text-align: center; padding-top: 7px; padding-bottom: 7px;" ><p>  </p> </td>
                <td style="width: 33%; text-align: right; padding-top: 7px; padding-bottom: 7px;"><p>  {DATE d/m/Y}  </p> </td>
            </tr>
            <tr style="width: 100%">
                <td style="width: 33%; text-align: left; padding-top: 7px; padding-bottom: 7px;"><p> +44 (0) 121 573 0081 </p> </td>
                <td style="width: 33%; text-align: center; padding-top: 7px; padding-bottom: 7px;"><p> info@sheshtech.com </p> </td>
                <td style="width: 33%; text-align: right; padding-top: 7px; padding-bottom: 7px;"><p> www.sheshtech.com </p> </td>
            </tr>
        </table>
        <p style="margin-top: 5px; font-size: 12px" >Shesh Tech is a trading name of <span style="color: #5393ea">Clements Innovations Ltd</span>. Registered in England and Wales. Company No. 10837349 </p> 
    </htmlpagefooter>

        <p>
            {!!nl2br($description)!!}
        </p>
        
        @for ($i = 0; $i < count($data); $i++)
            <h2>{{$data[$i]['heading']}} </h2>
            <table class="body-table"  >
                @for ($f = 0; $f < count($data[$i]['fields']); $f++)
                <tr>
                    <td class="left">
                        <p>{{key($data[$i]['fields'][$f])}}</p>
                    </td> 
                    <td  class="right">
                        <p>{{$data[$i]['fields'][$f][key($data[$i]['fields'][$f])] }}</p>
                    </td>
                </tr>     
                @endfor
            </table>
        @endfor
    </body> 
</html> 

