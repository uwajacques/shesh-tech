@extends('layouts.app')

@section('content')
<div class="container">

    
<form class="user-form" style=" padding: 70px 0px;" method="POST" action="">
        @if (session()->has('success'))
            <div class="alert alert-success" role="alert">      
                User Updated!
            </div>
        @endif


        @csrf
        <input type="hidden" name="id" value="{{$user->id}}"> 
        <div class="form-row">
            <div class="form-group col-md-12">
                <label for="inputName">Full Name</label>
            <input type="text" name="name" class="form-control" id="inputName" value="{{ $user->name }}" placeholder="Full Name">
            @if ($errors->has('name'))
                <div class="error"> {{ $errors->first('name') }}</div>
            @endif
            </div>
        </div>

        <div class="form-row">
            
            <div class="form-group col-md-6">
                <label for="inputEmail">Email</label>
                <input type="email" name="email" class="form-control" id="inputEmail" value="{{ $user->email }}" placeholder="Email">
                @if ($errors->has('email'))
                    <div class="error"> {{ $errors->first('email') }}</div>
                @endif
            </div>

            <div class="form-group col-md-6">
                <label for="inputPassword4">Password</label>
                <input type="password" name="password" class="form-control" id="inputPassword4" value="" placeholder="Password">
                @if ($errors->has('password'))
                    <div class="error"> {{ $errors->first('password') }}</div>
                @endif
            </div>
        </div>

        <div class="form-group">
            <div class="form-check">
            <input class="form-check-input" name="userType" type="checkbox" {{ ( $user->userType == 'admin'  ) ? 'checked' : '' }}  id="gridCheck">
            <label class="form-check-label" for="gridCheck">
                Admin User
            </label>
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Update User</button>
    </form>
</div>
@endsection
