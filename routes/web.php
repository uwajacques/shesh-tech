<?php
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return redirect('/home');
});

Auth::routes();
Route::get('/report', 'GeneratorCtrl@create');
Route::post('/report', 'GeneratorCtrl@generate');
Route::post('/gen', 'GeneratorCtrl@generate');
Route::get('/generate', 'GeneratorCtrl@generate');

Route::get('/delete/{id}', 'GeneratorCtrl@delete');
Route::get('/edit/{id}', 'GeneratorCtrl@edit');
Route::post('/edit/{id}', 'GeneratorCtrl@postEdit');


Route::get('/home', 'HomeController@index');
Route::get('/users', 'HomeController@users');
Route::get('/user/{id}', 'HomeController@user');
Route::post('/user/{id}', 'HomeController@updateUser');
Route::get('/new/user', 'HomeController@newUser');
Route::post('/new/user', 'HomeController@addUser');
