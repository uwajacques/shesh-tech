<?php

namespace App\Http\Controllers;

use Hash;
use \App\User;
use \App\Report;
use Validator;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reports = Report::where('location_path', '!=', '')->orderBy('id', 'desc')->paginate(15);
        return view('home', compact('reports'));
    }

    public function users(){
        $users = User::all();
        return view('users', compact('users'));
    }

    public function user($id){
         $user = User::find($id);
        return view('users.update-user', compact('user'));
    }

    public function updateUser(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $user = User::find($request->id);

        if( $user ){
            $user->name = $request->name;
            $user->email = $request->email;
            $user->userType = $request->has('userType') ? 'admin' : 'user';

            if(!empty($request->password)){
                $user->password = Hash::make($request->password);
            }
            
            $user->save();
            return redirect()->back()->with('success', true);   
        }

        return redirect()->back()->with('success', false );   
    }

    public function addUser(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|unique:users',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('new/user')
                        ->withErrors($validator)
                        ->withInput();
        }

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->userType = $request->has('userType') ? 'admin' : 'user';

        if($user->save()){
            return redirect('users');
        }
    }

    public function newUser(){
        return view('users.new-user');
    }
}
