<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = ['name', 'fields', 'description', 'generated_by', 'location_path'];
}
